# whitepaper_wikidata

## Name
Traduction du white paper de l'ARL : ["White paper on Wikidata"](https://www.arl.org/resources/arl-whitepaper-on-wikidata/) 

## Description
Cette traduction est quasi brut via Deepl.com, et sans aucun doute améliorable. Je n'ai pas lancé la traduction sur le sommaire, ni les notes. La mise en page a pu être abîmée mais le sens est là.

Ce document me semblait important dans une période où l'on parle beaucoup de websémantique dans les GLAM en France. Mon anglais étant moyen, avoir ce document en français m'étais utile, alors le voilà repartagé.

## Usage
La traduction de ce document pourrait être utile dans les échanges à venir sur la transition "archivistique" sur le modèle de la transition bibliographique, et le choix par l'Abes et BnF de proposer créer une wikibase pour le FNE.

## Roadmap
Comme mentionné plus haut la traduction et la mise en page peuvent largement être améliorée, donc avis aux volontaires.

## License
CC-BY

## Project status
V 1.0
